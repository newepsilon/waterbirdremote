(var q (require "q"))
(var fsp (require "fs-promise"))
(var mp (require "mongodb-promise"))
(var path (require "path"))
(var sjcl (require "sjcl"))
(var password "wtf")
(var intake (function () (q.all (-> fsp (.readdir "scans") (.invoke "map" (function (file) (-> q (.all (array (q (file.match "(^[^.]*).([^.]*$)")) (-> fsp (.stat (path.join "scans" file)) (.get "ctime")) (fsp.readFile (path.join "scans" file)))) (.then (function (data) (-> mp.MongoClient (.connect "mongodb://127.0.0.1:27017/docs") (.then (function (db) (-> db (.collection "scans") (.then (function (collection) (-> collection (.insert (object "name" (sjcl.encrypt password data[0][1]) "extension" (sjcl.encrypt password data[0][2]) (sjcl.encrypt password "tstamp" data[1]) "data" (sjcl.encrypt password data[2]))) (.then (function () (db.close))) (.then (function () (fsp.unlink (path.join "scans" file))))))))))))))))))))
(-> (require "node-crontab") (.scheduleJob "* * * * *" intake) )
